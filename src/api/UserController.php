<?php

namespace Onyx\User\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Onyx\Api\ApiController;
use Onyx\Log\LogController;
use Onyx\User\Models\Resources\UserResource;
use Onyx\User\Models\User;

class UserController extends ApiController
{

    /**
     * Return all Users as a json data
     *
     * @param Request $request
     * @return JsonResponse OK Json Response if authorized
     */
    public function index(Request $request): JsonResponse
    {

        $users = User::all();

        $data = ['users' => UserResource::collection($users)];


        return $this->respondOk($data, 'All Users.');
    }


    /**
     * Return specific User by userId
     *
     * @param Request $request Data contains token
     * @param int $userId User to return
     * @return JsonResponse OK Json Response if authorized and exists
     */
    public function get(Request $request, int $userId): JsonResponse
    {

        $user = User::where('id', $userId)->first();

        if(!$user){
            return $this->respondNotFound('User not found.');
        }

        $userResource = new UserResource($user);
        return $this->respondOk(['user' => $userResource], 'User found.');
    }


    /**
     * Create a new User via API.
     *
     * @param Request $request Data contains token
     * @return JsonResponse OK Json Response if authorized
     */
    public function insert(Request $request): JsonResponse
    {

        if (!$this->getLoggedUser($request)) {
            return $this->respondUnauthorized();
        }

        $data = $request->only('title_before', 'first_name', 'last_name', 'title_after', 'role_id', 'email', 'password', 'phone', 'active', 'api_token', 'note', 'remember_token');

        $user = User::create($data);
        $userResource = new UserResource($user);

        LogController::insert($this->getLoggedUser($request)->id, 1, 'User successfully created.');

        return $this->respondOk(['user' => $userResource], 'User successfully created.');

    }


    /**
     * Update given User
     *
     * @param Request $request User data and token
     * @param int $userId User to update
     * @return JsonResponse OK Json Response if authorized and exists
     */
    public function update(Request $request, int $userId): JsonResponse
    {

        if (!$this->getLoggedUser($request)) {
            return $this->respondUnauthorized();
        }

        $user = User::where('id', $userId)->first();

        if (!$user) {
            return $this->respondNotFound('User for update not found.');
        }

        $data = $request->only('title_before', 'first_name', 'last_name', 'title_after', 'role_id', 'email', 'password', 'phone', 'active', 'api_token', 'note', 'remember_token');

        if ($user->update($data)) {

            $userResource = new UserResource($user);

            LogController::insert($this->getLoggedUser($request)->id, 1, 'User (' . $user->id . ') updated.');
            return $this->respondOk(['user' => $userResource], 'User (ID ' . $user->id . ') updated.');

        } else {

            LogController::insert($this->getLoggedUser($request)->id, 1, 'User (' . $user->id . ') update error.');
            return $this->respondInternalError('User (ID ' . $user->id . ') update error.');
        }
    }


    /**
     * Delete given User
     *
     * @param Request $request Data contains token
     * @param int $userId User to delete
     * @return JsonResponse OK Json Response if authorized and User exists
     */
    public function delete(Request $request, int $userId): JsonResponse
    {

        if (!$this->getLoggedUser($request)) {
            return $this->respondUnauthorized();
        }

        $user = User::where('id', $userId)->first();

        if (!$user) {
            return $this->respondNotFound('User to delete not found.');
        }

        try {
            $user->delete();

            LogController::insert($this->getLoggedUser($request)->id, 1, 'User ID '. $user->id .' successfully deleted.');
            return $this->respondOk(['success' => 'User deleted.'], 'User ID '. $user->id .' successfully deleted.');
        }catch (\Exception $e){

            LogController::insert($this->getLoggedUser($request)->id, 1, 'User ID '. $user->id .' delete error.');
            return $this->respondInternalError('User ID '. $user->id .' delete error.');
        }

    }


}
