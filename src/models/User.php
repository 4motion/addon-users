<?php

namespace Onyx\User\Models;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Onyx\Import\Models\Import;
use Onyx\Product\Models\Product;
use Onyx\Role\Models\Role;


/**
 * Onyx\User\Models\User
 *
 * @method static create(array $data): Onyx\User\Models\User
 * @property int $id
 * @property string|null $title_before
 * @property string $first_name
 * @property string $last_name
 * @property string|null $title_after
 * @property string $email
 * @property Role $role_id
 * @property Carbon|null $email_verified_at
 * @property string|null $note
 * @property string $password
 * @property string|null $api_token
 * @property string|null $phone
 * @property boolean $active
 * @mixin Eloquent
 */
class User extends Authenticatable
{

    use HasFactory, Notifiable, SoftDeletes, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title_before',
        'first_name',
        'last_name',
        'title_after',
        'role_id',
        'email',
        'password',
        'phone',
        'active',
        'note'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'api_token'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

//  Get Full name of User
    public function getFullName(): string
    {
        return $this->first_name ." ". $this->last_name;
    }

    public function getFullNameRevert(): string
    {
        return $this->last_name ." ". $this->first_name;
    }

    /**
     * Relationship to roles.
     *
     * @return BelongsTo
     */
    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    public function import(): BelongsToMany
    {
        return $this->belongsToMany(Import::class);
    }

    /**
     * Sets api_token to random hash and saves user
     */
    public function setApiToken()
    {
        $randomStr = Str::random(20);
        $this->api_token = Hash::make($randomStr, ['length' => 40]);

        $this->save();
    }
}
