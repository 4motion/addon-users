<?php

namespace Onyx\User\Models\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;

class UserResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    #[ArrayShape(['id' => "mixed", 'title_before' => "mixed", 'first_name' => "mixed", 'last_name' => "mixed",
        'title_after' => "mixed", 'role_id' => "mixed", 'email' => "mixed", 'password' => "mixed", 'email_verified_at' => "mixed",
        'phone' => "mixed", 'active' => "mixed", 'api_token' => "mixed", 'note' => "mixed"])]
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title_before' => $this->title_before,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'title_after' => $this->title_after,
            'role_id' => $this->role_id,
            'email' => $this->email,
            'password' => $this->password,
            'email_verified_at' => $this->email_verified_at,
            'phone' => $this->phone,
            'active' => $this->active,
            'api_token' => $this->api_token,
            'note' => $this->note
        ];
    }
}
