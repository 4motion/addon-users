<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->id();
            $table->string('title_before', 200)->nullable(); //titul před jménem
            $table->string('first_name', 200);
            $table->string('last_name', 200);
            $table->string('title_after', 200)->nullable(); //titul za jménem
            $table->foreignId('role_id')->constrained('roles')->index('role_id')->unsigned();
            $table->string('email')->unique();
            $table->text('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone', 100)->nullable();
            $table->boolean('active')->default(true);
            $table->string('api_token')->default(null)->nullable(); //API token for authorization
            $table->text('note')->nullable(); //poznámka k uživateli

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
