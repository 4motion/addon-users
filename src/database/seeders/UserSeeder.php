<?php

namespace Onyx\User\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Onyx\Role\Models\Role;
use Onyx\User\Models\News;
use Onyx\User\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //migration of 4motion users
        $fourMotionUsers = array(

            //4MD users

            array('UserTitleBefore' => '','UserTitleAfter' => 'DiS.','UserName' => 'System','UserSurname' => 'MZMB','UserLogin' => 'system@mzmb.cz','UserPhone' => '999999999','UserCompanyID' => '4','UserBranchID' => '4','UserRole' => 'Administrátor','Function' => 'Programátor', 'GOOGLE_ID' => null),
            array('UserTitleBefore' => '','UserTitleAfter' => 'Mgr.','UserName' => 'Bohuslav','UserSurname' => 'Coufal','UserLogin' => 'bcoufal@onyx.cz','UserPhone' => '999999999','UserCompanyID' => '4','UserBranchID' => '4','UserRole' => 'Administrátor','Function' => 'Programátor', 'GOOGLE_ID' => null),
            array('UserTitleBefore' => '','UserTitleAfter' => 'DiS.','UserName' => 'Tomáš','UserSurname' => 'Havlíček','UserLogin' => 'tomas.havlicek@4motion.cz','UserPhone' => '+420604380628','UserCompanyID' => '4','UserBranchID' => '4','UserRole' => 'Administrátor','Function' => 'Programátor', 'GOOGLE_ID' => '101477465384490698195'),
            array('UserTitleBefore' => '','UserTitleAfter' => '','UserName' => 'Tomáš','UserSurname' => 'Klusoň','UserLogin' => 'kluson@4motion.cz','UserPhone' => '+420722653171','UserCompanyID' => '4','UserBranchID' => '4','UserRole' => 'Administrátor','Function' => 'Programátor', 'GOOGLE_ID' => null),
            array('UserTitleBefore' => 'Ing.','UserTitleAfter' => '','UserName' => 'Ondřej','UserSurname' => 'Malý','UserLogin' => 'maly@4motion.cz','UserPhone' => '+420603297468','UserCompanyID' => '4','UserBranchID' => '4','UserRole' => 'Administrátor','Function' => 'Programátor', 'GOOGLE_ID' => null),
            array('UserTitleBefore' => '','UserTitleAfter' => '','UserName' => 'Denis','UserSurname' => 'Ferda','UserLogin' => 'ferda@4motion.cz','UserPhone' => '+420773677046','UserCompanyID' => '13','UserBranchID' => '4','UserRole' => 'Administrátor','Function' => 'Programátor', 'GOOGLE_ID' => null),
            array('UserTitleBefore' => '','UserTitleAfter' => '','UserName' => 'Jan','UserSurname' => 'Tesař','UserLogin' => 'tesar@4motion.cz','UserPhone' => '+420773677046','UserCompanyID' => '4','UserBranchID' => '4','UserRole' => 'Administrátor','Function' => 'Programátor', 'GOOGLE_ID' => null),
            array('UserTitleBefore' => '','UserTitleAfter' => '','UserName' => '4MD','UserSurname' => 'TEST','UserLogin' => 'info@4motion.cz','UserPhone' => '','UserCompanyID' => '4','UserBranchID' => '24','UserRole' => 'Uživatel','Function' => 'Programátor', 'GOOGLE_ID' => null),
            array('UserTitleBefore' => 'Ing.','UserTitleAfter' => '','UserName' => 'Vít','UserSurname' => 'Skopal','UserLogin' => 'skopal@4motion.cz','UserPhone' => '','UserCompanyID' => '4','UserBranchID' => '24','UserRole' => 'Administrátor','Function' => 'Programátor', 'GOOGLE_ID' => null),
            array('UserTitleBefore' => '','UserTitleAfter' => '','UserName' => 'Petr','UserSurname' => 'Mašát','UserLogin' => 'masat@4motion.cz','UserPhone' => '','UserCompanyID' => '4','UserBranchID' => '24','UserRole' => 'Administrátor','Function' => 'Programátor', 'GOOGLE_ID' => null),

            array('UserTitleBefore' => '','UserTitleAfter' => '','UserName' => 'Petr','UserSurname' => 'Manazer','UserLogin' => 'manazer@4motion.cz','UserPhone' => '','UserCompanyID' => '4','UserBranchID' => '24','UserRole' => 'Manažer','Function' => 'Programátor', 'GOOGLE_ID' => null),
            array('UserTitleBefore' => '','UserTitleAfter' => '','UserName' => 'Pavel','UserSurname' => 'Uzivatel','UserLogin' => 'uzivatel@4motion.cz','UserPhone' => '','UserCompanyID' => '4','UserBranchID' => '24','UserRole' => 'Uživatel','Function' => 'Programátor', 'GOOGLE_ID' => null),
            array('UserTitleBefore' => '','UserTitleAfter' => '','UserName' => 'Jan','UserSurname' => 'Kurator','UserLogin' => 'kurator@4motion.cz','UserPhone' => '','UserCompanyID' => '4','UserBranchID' => '24','UserRole' => 'Kurátor','Function' => 'Programátor', 'GOOGLE_ID' => null)

        );


        foreach ($fourMotionUsers as $key => $value) {

            $unhashedPassword = "4md-".$value['UserSurname'];

            $randomStr = Str::random(20);
            $token = Hash::make($randomStr, ['length' => 40]);

            $userModel = new User();
            $userModel->setAttribute('role_id', Role::where("name_cs", $value['UserRole'])->first()->id);
            $userModel->setAttribute('email', $value['UserLogin']);
            $userModel->setAttribute('email_verified_at', Carbon::now());
            $userModel->setAttribute('password', Hash::make($unhashedPassword));
            $userModel->setAttribute('first_name', $value['UserName']);
            $userModel->setAttribute('last_name', $value['UserSurname']);
            $userModel->setAttribute('active', true);
            $userModel->setAttribute('remember_token', Str::random(10));
            $userModel->setAttribute('api_token', $token);


            $userModel->save();
        }

    }
}
