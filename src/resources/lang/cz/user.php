<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User translations
    |--------------------------------------------------------------------------
    */

    'usersTitle' => 'Uživatelé',
    'usersIndexSub' => 'Seznam uživatelů',
    'usersCreateSub' => 'Vytvoření uživatele',
    'usersEditSub' => 'Editace uživatele',
    'usersProfile' => 'Můj profil',

    'usersChangePassword' => 'Změna hesla',
    'usersChangePasswordBtn' => 'Změnit heslo',
    'usersCurrentPassword' => 'Současné heslo',
    'usersNewPassword' => 'Nové Heslo',
    'usersNewPasswordConfirmation' => 'Nové heslo znovu',
    'usersNewPleaseFillNewPassword' => 'Vyplňte prosím nové heslo',
    'usersNewPleaseFillCurrentPassword' => 'Vyplňte prosím současné heslo',
    'usersPasswordRequirement' => 'Heslo musí být minimálně 8 znaků dlouhé a musí obsahovat minimálně jednu číslici a speciální znak',

    'usersPhCurrentPassword' => 'Zadejte současné heslo',
    'usersPhNewPassword' => 'Zadejte nové heslo',
    'usersPhNewPasswordConfirmation' => 'Zadejte nové heslo znovu',

];
