<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User translations
    |--------------------------------------------------------------------------
    */

    'usersTitle' => 'Users',
    'usersIndexSub' => 'Select users',
    'usersCreateSub' => 'Create user',
    'usersEditSub' => 'Edit user',

];
