<div class="card shadow">
    <div class="card-header border-0 pt-5">
        <h3 class="card-title align-items-start flex-column">
            <span class="card-label fw-bolder fs-3 mb-1">Naposledy přidaní uživatelé</span>
        </h3>
        <div class="card-toolbar">
            @if(\Illuminate\Support\Facades\Auth::user()->role->user_create)
                <a href="{{ route('users.create') }}" class="btn btn-sm btn-light-primary me-2">
                    <i class="fa fa-plus-circle"></i> Nový uživatel
                </a>
                <a href="{{ route('users.index') }}" class="btn btn-sm btn-primary float-right fw-bolder">
                    <i class="bi bi-person"></i> Všichni uživatelé
                </a>
            @endif
        </div>
    </div>

    <div class="separator my-5"></div>

    <div class="card-body py-3">
        <div class="table-responsive">
            <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                <thead>
                <tr class="fw-bolder text-muted">
                    <th class="min-w-130px">Jméno</th>
                    <th class="min-w-130px">Příjmení</th>
                    <th class="min-w-130px">Role</th>
                    <th class="min-w-130px">E-mail</th>
                    <th class="min-w-130px"></th>
                </tr>
                </thead>

                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>
                            <div class="d-flex flex-column w-100 me-2">
                                <div class="d-flex flex-stack mb-2">
                                        <span class="text-muted me-2 fs-7 fw-boldest text-gray-700">
                                            {{ $user->first_name }}
                                        </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="d-flex flex-column w-100 me-2">
                                <div class="d-flex flex-stack mb-2">
                                        <span class="text-muted me-2 fs-7 fw-boldest text-gray-700">
                                            {{ $user->last_name }}
                                        </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="d-flex flex-column w-100 me-2">
                                <div class="d-flex flex-stack mb-2">
                                        <span class="me-2 fs-7 fw-bold">
                                            <span class="badge {{ $user->role->id == 1 ? "badge-light-danger" : "badge-light-primary"}}">{{ $user->role->name_cs }}</span>
                                        </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="d-flex flex-column w-100 me-2">
                                <div class="d-flex flex-stack mb-2">
                                        <span class="text-muted me-2 fs-7 fw-bold">
                                            {{ $user->email }}
                                        </span>
                                </div>
                            </div>
                        </td>
                        <td class="text-end">
                            @if(\Illuminate\Support\Facades\Auth::user()->role->user_edit)
                                <a href="{{ route('users.edit',$user->id) }}" class="btn btn-icon btn-bg-light btn-active-color-info btn-sm me-1">
                                    <span class="svg-icon svg-icon-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" fill="black"></path>
                                        <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" fill="black"></path>
                                        </svg>
                                    </span>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
