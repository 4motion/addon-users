@section('page_title')
    {{ $pageTitle .' |' }}
@endsection
<x-base-layout>

    @if(\Illuminate\Support\Facades\Auth::user()->role->user_index == 0)
        <div class="card d-flex justify-content-center" style="width: 100%;height: 100%;">
            <div style="margin: 0 auto;" class="d-flex flex-column justify-content-center">
                <img src="{{ asset(theme()->getMediaUrlPath() . 'logos/logo.png') }}" width="300" alt="" style="margin: auto !important;">
                <h2 class="fw-boldest text-gray-700 mt-5">Nemáte oprávnění k zobrazení této strany!</h2>
            </div>
        </div>
    @else

    <div class="card shadow p-3">
        <div class="d-flex justify-content-between">

            <div class="d-flex align-items-center position-relative my-3 ms-5">
                <span class="svg-icon svg-icon-1 position-absolute ms-4"><i class="fa fa-search"></i></span>
                <input type="text" data-kt-filter="search" class="form-control form-control-solid w-350px ps-14" placeholder="Hledat uživatele..." id="search"/>
            </div>

            <div></div>

            <div class="card-toolbar mt-4 me-5">
                @if(\Illuminate\Support\Facades\Auth::user()->role->user_create)
                <a href="{{ route('users.create') }}" class="btn btn-sm btn-primary">
                    <i class="fa fa-plus-circle"></i> Vytvořit uživatele
                </a>
                @endif
            </div>
        </div>
    </div>

    <div class="card shadow mt-10">
        <div class="table-responsive">
            <table class="table table-striped table-row-bordered gy-5 gs-7" id="tbl-users">
                <thead>
                <tr class="fw-bolder text-muted">
                    <th class="min-w-130px">Jméno</th>
                    <th class="min-w-130px">Příjmení</th>
                    <th class="min-w-130px">Role</th>
                    <th class="min-w-130px">E-mail</th>
                    <th class="min-w-130px"></th>
                </tr>
                </thead>

                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>
                            <div class="d-flex flex-column w-100 me-2">
                                <div class="d-flex flex-stack mb-2">
                                        <span class="me-2 fs-7 fw-bold">
                                            {{ $user->first_name }}
                                        </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="d-flex flex-column w-100 me-2">
                                <div class="d-flex flex-stack mb-2">
                                        <span class="me-2 fs-7 fw-bold">
                                            {{ $user->last_name }}
                                        </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="d-flex flex-column w-100 me-2">
                                <div class="d-flex flex-stack mb-2">
                                        <span class="me-2 fs-7 fw-bold">
                                            <span class="badge {{ $user->role->id == 1 ? "badge-light-danger" : "badge-light-primary"}}">{{ $user->role->name_cs }}</span>
                                        </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="d-flex flex-column w-100 me-2">
                                <div class="d-flex flex-stack mb-2">
                                        <span class="me-2 fs-7 fw-bold">
                                            {{ $user->email }}
                                        </span>
                                </div>
                            </div>
                        </td>
                        <td class="text-end">
                            @if(\Illuminate\Support\Facades\Auth::user()->role->user_edit)
                            <a href="{{ route('users.edit',$user->id) }}" class="btn btn-icon btn-bg-light btn-active-color-info btn-sm me-1">
                                <span class="svg-icon svg-icon-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" fill="black"></path>
                                    <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" fill="black"></path>
                                    </svg>
                                </span>
                            </a>
                            @endif
                            @if(\Illuminate\Support\Facades\Auth::user()->role->user_delete)
                            <button type="button" class="btn btn-icon btn-bg-light btn-active-color-danger btn-sm deleteUser" id="delete-{{$user->id}}-{{$user->getFullNameRevert()}}">
                                <span class="svg-icon svg-icon-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="black"></path>
                                        <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="black"></path>
                                        <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="black"></path>
                                    </svg>
                                </span>
                            </button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @endif

</x-base-layout>

<script>

    $(document).ready(function () {

        const table = $("#tbl-users").DataTable({
            "paging": false,
            "bInfo": false,
            "dom": "<'table-responsive'tr>",
            "language": {
                "lengthMenu": "Zobrazeno -- záznamů na stránce",
                "zeroRecords": "Nebyly nalezeny žádné data.",
                "infoEmpty": "Doposud nebyly zadány žádné importy"
            },
        });

        $('#search').on('keyup', function () {
            table.search(this.value).draw();
        });

        $(".deleteUser").click(function () {

            let userArray = $(this).attr('id').split('-');
            let userDeleteId = userArray[1];
            let userName = userArray[2];
            const userId = @json(\Illuminate\Support\Facades\Auth::id());

            if (userId == userDeleteId){

                Swal.fire({
                    title: "Není možné smazat sám sebe.",
                    icon: "warning",
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 1500,
                })

            }else{

                Swal.fire({
                    title: 'Opravdu si přejete smazat uživatele '+ userName +'?',
                    text: 'Tato operace je nevratná',
                    icon: "question",
                    showDenyButton: true,
                    showCancelButton: false,
                    showConfirmButton: true,
                    confirmButtonText: "<i class='fa fa-trash text-white'></i> Ano",
                    denyButtonText: "<i class='fa fa-thumbs-down'></i> Ne",
                    width: '30%',
                    customClass: {
                        confirmButton: 'btn btn-danger',
                        denyButton: 'btn btn-secondary'
                    }}).then((result) => {

                    if (result.isConfirmed) {

                        $.ajax({
                            url: "/ajaxDeleteUser",
                            type: "POST",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                userDeleteId: userDeleteId,
                                userId: userId
                            },
                            success: function (response) {

                                if(response == "1"){

                                    Swal.fire({
                                        title: "Uživatel "+ userName +" úspěšně smazán.",
                                        icon: "success",
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 1500,
                                    }).then((function () {
                                        location.reload();
                                    }));
                                }else{

                                    Swal.fire({
                                        title: "Uživatel "+ userName +" nebyl smazán.",
                                        icon: "error",
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 1500,
                                    }).then((function () {
                                        location.reload();
                                    }));
                                }
                            },
                            error: function (response) {

                                Swal.fire({
                                    title: "Uživatel "+ userName +" nebyl smazán.",
                                    icon: "error",
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 1500,
                                }).then((function () {
                                    location.reload();
                                }))
                                console.log(response);
                            },
                        });

                    } else if (result.isDenied) {
                        Swal.fire({
                            title: "Uživatel "+ userName +" nesmazána",
                            position: 'top-end',
                            icon: "info",
                            showDenyButton: false,
                            showCancelButton: false,
                            showConfirmButton: false,
                            timer: 1000,
                        });
                    }
                });
            }

        });

    });

</script>
