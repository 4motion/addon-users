@section('page_title')
    {{ $pageTitle .' |' }}
@endsection
<x-base-layout>

    @if(\Illuminate\Support\Facades\Auth::user()->role->user_edit == 0)
        <div class="card d-flex justify-content-center" style="width: 100%;height: 100%;">
            <div style="margin: 0 auto;" class="d-flex flex-column justify-content-center">
                <img src="{{ asset(theme()->getMediaUrlPath() . 'logos/logo.png') }}" width="300" alt="" style="margin: auto !important;">
                <h2 class="fw-boldest text-gray-700 mt-5">Nemáte oprávnění k zobrazení této strany!</h2>
            </div>
        </div>
    @else

    @if (count($errors) > 0)
    <!--begin::Alert-->
        <div class="alert alert-danger">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column">
                <!--begin::Title-->
                <h4 class="mb-1 text-danger mb-5">Něco nám tu chybí</h4>
                <!--end::Title-->
                <!--begin::Content-->
                <ul>
                    @foreach ($errors->all() as $error)
                        <li class="text-danger">{{ $error }}</li>
                    @endforeach
                </ul>
                <!--end::Content-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Alert-->
    @endif

    <form method="POST" class="form" action="{{ route('users.update', $user) }}" enctype="multipart/form-data" >
        @csrf
        @method('PUT')
        <div class="card shadow">

            <div class="card-header d-flex justify-content-between py-5 px-5">
                <a href="{{ route('users.index') }}" class="btn btn-secondary font-weight-bolder shadow-sm">
                    <i class="fa fa-arrow-circle-left"></i> zpět
                </a>
                <div class="mt-4"><h2 class="text-gray-700">Editace uživatele {{ $user->getFullName() }}</h2></div>
                <button type="submit" class="btn btn-primary font-weight-bolder shadow-sm btn-hover-scale" name="submitForm" id="submitForm">
                    <i class="fa fa-save fw-boldest" id="btnSave"></i>
                    <i class="fa fa-check d-none fw-boldest" id="btnSaveCheck"></i>
                    Uložit
                </button>
            </div>

            <div class="card-body px-0">
                <div class="tab-content pt-5">
                    <div class="tab-pane active" id="kt_apps_contacts_view_tab_2" role="tabpanel">
                        <div class="form-group row">
                            <div class="form-group row mb-10">
                                <label class="col-lg-3 col-form-label fw-boldest text-gray-700 required" style="text-align: right;">Jméno</label>
                                <div class="col-lg-6">
                                    <input type="text" name="first_name" class="form-control form-control-solid" value="{{ old('first_name', $user->first_name) }}" placeholder="Zadejte křestní jméno">
                                    @error('first_name')
                                    <span class="text-danger">Vyplňte prosím jméno</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-10">
                                <label class="col-lg-3 col-form-label fw-boldest text-gray-700 required" style="text-align: right;">Příjmení</label>
                                <div class="col-lg-6">
                                    <input type="text" name="last_name" class="form-control form-control-solid" value="{{ old('last_name', $user->last_name) }}" placeholder="Zadejte příjmení">
                                    @error('last_name')
                                    <span class="text-danger">Vyplňte prosím přijmení</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-10">
                                <label class="col-lg-3 col-form-label fw-boldest text-gray-700" style="text-align: right;">Titul před</label>
                                <div class="col-lg-6">
                                    <input type="text" name="title_before" class="form-control form-control-solid" value="{{ old('title_before', $user->title_before) }}" placeholder="Zadejte titul před jménem">
                                </div>
                            </div>
                            <div class="form-group row mb-10">
                                <label class="col-lg-3 col-form-label fw-boldest text-gray-700" style="text-align: right;">Titul za</label>
                                <div class="col-lg-6">
                                    <input type="text" name="title_after" class="form-control form-control-solid" value="{{ old('title_after', $user->title_after) }}" placeholder="Zadejte titul za jménem">
                                </div>
                            </div>
                            <div class="form-group row mb-10">
                                <label class="col-lg-3 col-form-label fw-boldest text-gray-700 required" style="text-align: right;">Role</label>
                                <div class="col-lg-6">
                                    <select class="form-control form-select form-select-solid" name="role_id" id="role_id">
                                        <option value="" disabled selected>Vyberte roli</option>
                                        @foreach($roles as $role)
                                            <option {{ $role->id === old('role_id') ? "selected" : "" }} value="{{ $role->id }}">
                                                {{ $role->name_cs }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-10">
                                <label class="col-lg-3 col-form-label fw-boldest text-gray-700 required" style="text-align: right;">E-mail</label>
                                <div class="col-lg-6">
                                    <input type="email" name="email" class="form-control form-control-solid" value="{{ old('email', $user->email) }}"  placeholder="Zadejte email" autocomplete="off">
                                </div>
                                @error('email')
                                <span class="text-danger">Vyplňte prosím email</span>
                                @enderror
                            </div>
                            <div class="form-group row mb-10">
                                <label class="col-lg-3 col-form-label fw-boldest text-gray-700 required" style="text-align: right;">Heslo</label>
                                <div class="col-lg-6">
                                    <!--begin::Main wrapper-->
                                    <div class="fv-row" data-kt-password-meter="true">
                                        <!--begin::Wrapper-->
                                        <div class="mb-1">
                                            <!--begin::Input wrapper-->
                                            <div class="position-relative mb-3">
                                                <input class="form-control form-control-lg form-control-solid"
                                                       type="password" placeholder="Zadejte heslo" name="password" autocomplete="off" value=""/>
                                                <!--begin::Visibility toggle-->
                                                <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2"
                                                      data-kt-password-meter-control="visibility">
                                                <i class="bi bi-eye-slash fs-2"></i>

                                                <i class="bi bi-eye fs-2 d-none"></i>
                                            </span>
                                                <!--end::Visibility toggle-->
                                            </div>
                                            <!--end::Input wrapper-->
                                            <!--begin::Highlight meter-->
                                            <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                                            </div>
                                            <!--end::Highlight meter-->
                                        </div>
                                        <!--end::Wrapper-->

                                        <!--begin::Hint-->
                                        <div class="text-muted">
                                            Heslo musí být minimálně 8 znaků dlouhé a musí obsahovat minimálně jednu číslici a speciální znak
                                        </div>
                                        <!--end::Hint-->
                                        <div class="separator my-2"></div>
                                        <!--begin::Hint-->
                                        <div class="text-muted">
                                            Jestli si nepřejete měnit stávající heslo, ponechte pole prázdné
                                        </div>
                                        <!--end::Hint-->
                                    </div>
                                    <!--end::Main wrapper-->
                                    @error('password')
                                    <span class="text-danger">Vyplňte prosím heslo</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-10">
                                <label class="col-lg-3 col-form-label fw-boldest text-gray-700 required" style="text-align: right;">Telefon</label>
                                <div class="col-lg-6">
                                    <input type="text" name="phone" class="form-control form-control-solid" id="phone" value="{{ old('phone', $user->phone) }}" placeholder="Zadejte telefonní číslo">
                                    @error('phone')
                                    <span class="text-danger">Vyplňte prosím telefon</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-10">
                                <label class="col-lg-3 col-form-label fw-boldest text-gray-700 required" style="text-align: right;">Aktivní</label>
                                <div class="col-lg-6">
                                    <div class="form-check form-switch form-check-custom form-check-solid">
                                        <input name="active" class="form-check-input" type="checkbox" id="flexSwitchChecked" {{ old("active", $user->active) ? "checked" : "" }} />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-10">
                                <label class="col-lg-3 col-form-label fw-boldest text-gray-700" style="text-align: right;">Poznámka</label>
                                <div class="col-lg-6">
                                    <textarea id="form7" class="md-textarea form-control form-control-solid" rows="3" name="note" placeholder="Zadejte poznámku k uživateli">{{ old('note', $user->note) }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </form>

    @endif

</x-base-layout>

<script>

    $('#submitForm').mouseenter(function () {

        $('#btnSave').addClass('d-none');
        $('#btnSaveCheck').removeClass('d-none');

    });

    $('#submitForm').mouseleave(function () {

        $('#btnSave').removeClass('d-none');
        $('#btnSaveCheck').addClass('d-none');

    });

    if (@json(old("role_id", $user->role_id))){

        $("#role_id").val(@json(old("role_id", $user->role_id)));
    }

    Inputmask({
        "mask" : "999 999 999"
    }).mask("#phone");

</script>
