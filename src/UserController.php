<?php

namespace Onyx\User;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Onyx\Role\Models\Role;
use Onyx\User\Models\User;
use Illuminate\Support\Facades\Auth;
use Onyx\Log\LogController;


class UserController extends Controller
{

    /**
     * Ajax function of user role.
     */
    public function ajaxGetUserRole(Request $request)
    {

        return User::where("id", intval($request->get("userId")))->first()->role;
    }

    /**
     * Ajax function of delete User.
     */
    public function ajaxDeleteUser(Request $request): bool
    {

        $result = false;

        if ($request->get("userDeleteId") == $request->get("userId")){

            return false;
        }

        $user = User::where('id', $request->get("userDeleteId"))->first();

        if ($user->delete()){

            $result = true;
            LogController::insert($request->get("userId"), 1, 'User ID '. $user->id .' successfully deleted.');
        }else{

            LogController::insert($request->get("userId"), 1, 'User ID '. $user->id .' delete error.');
        }

        return $result;
    }


    /**
     * Index page of Users.
    */
    public function index(): Factory|View|Application
    {
        $users = User::all();

        $pageTitle = __('user::user.usersIndexSub');
        $pageSub = trans('usersIndexSub');


        return view('users::index', compact('pageTitle', 'pageSub', 'users'));
    }


    /**
     * Create page of Users.
     */
    public function create(): Factory|View|Application
    {
        $roles = Role::all();

        $pageTitle = __('user::user.usersCreateSub');
        $pageSub = trans('usersCreateSub');

        return view('users::create', compact('pageTitle', 'pageSub', 'roles'));
    }


    /**
     * Store method of Users.
     */
    public function store(Request $request): \Illuminate\Routing\Redirector|Application|\Illuminate\Http\RedirectResponse
    {

        $this->validateUser($request);
        $user = new User($request->only('title_before', 'first_name','last_name','title_after', 'role_id','email', 'phone', 'note'));

        $user->active = $request->active === "on" ? true : false;
        $user->password = Hash::make($request->get("password"));
        $user->setApiToken();

        $user->save();
        LogController::insert(Auth::id(), 1, 'User successfully created.');

        return redirect('admin/users');
    }


    /**
     * Edit page of Users.
     */
    public function edit($id): Factory|View|Application
    {
        $user = User::where('id', $id)->first();
        $roles = Role::all();

        $pageTitle = __('user::user.usersEditSub');
        $pageSub = trans('usersEditSub');


        return view('users::edit', compact('pageTitle', 'pageSub', 'user', 'roles'));
    }


    /**
     * Update method of Users.
     */
    public function update(Request $request, $id): \Illuminate\Routing\Redirector|Application|\Illuminate\Http\RedirectResponse
    {

        $validator = \Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'sometimes|nullable|min:8|max:100',
            'phone' => 'required|max:16',
        ]);

        $updateUser = User::where('id', $id)->first();

        $updateUser->update($request->only('title_before', 'first_name','last_name','title_after', 'role_id','email', 'phone', 'note'));
        $updateUser->active = $request->active === "on" ? true : false;

        if ($request->has('password') && $request->get('password') != null){
            $updateUser->password = Hash::make($request->get('password'));
        }

        $updateUser->save();
        LogController::insert(Auth::id(), 1, 'User (' . $updateUser->id . ') updated.');

        return redirect('admin/users');
    }


    /**
     * Delete method of Users.
     */
    public function destroy($idUser): \Illuminate\Http\RedirectResponse
    {
        $deleteUser = User::where('id', $idUser)->first();

        if($deleteUser) {
            $deleteUser->delete();
        }
        LogController::insert(Auth::id(), 1, 'User ID '. $deleteUser->id .' successfully deleted.');

        return redirect()->back();
    }

    private function validateUser(Request $request): void
    {
        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:8|max:100',
            'phone' => 'required|max:16|unique:users',
        ];
        $request->validate($rules);
    }

    /**
     * My profile function
    */
    public function profile(): Factory|View|Application
    {

        $pageTitle = __('user::user.usersProfile');

        return view('users::profile', compact('pageTitle'));
    }

    /**
     * Change password by User
     */
    public function userChangePassword(Request $request): \Illuminate\Http\RedirectResponse
    {

        if ($request->get('user_id') != Auth::id()){
            return Redirect::back()->withErrors(['msg' => 'Nemáte právo měnit heslo tomuto uživateli!']);
        }

        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => 'required|min:8|max:100|confirmed',
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator);
        }

        if (!Hash::check($request->get('old_password'), Auth::user()->password))
        {
            return Redirect::back()->withErrors(['msg' => 'Zadané současné heslo se neshoduje s vaším aktuálním heslem!']);
        }

        $user =  Auth::user();

        $user->password = Hash::make($request->get('password'));
        $user->save();

        return redirect('admin/userProfile')->with('message', 'Heslo bylo aktualizováno');
    }

}
