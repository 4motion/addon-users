<?php

use Illuminate\Support\Facades\Route;

Route::resource('users', Onyx\User\UserController::class);
Route::post('ajaxDeleteUser', 'Onyx\User\UserController@ajaxDeleteUser');
Route::post('ajaxGetUserRole', 'Onyx\User\UserController@ajaxGetUserRole');

Route::prefix('api')->group(function () {

    // display all users
    Route::get('user', [Onyx\User\Api\UserController::class, 'index']);

    // insert new users
    Route::post('user/create', [Onyx\User\Api\UserController::class, 'insert']);

    // display specific users by userId
    Route::get('user/{userId}', [Onyx\User\Api\UserController::class, 'get']);

    // update specific user by userId
    Route::put('user/{userId}', [Onyx\User\Api\UserController::class, 'update']);

    // delete specific user by userId
    Route::delete('user/{userId}', [Onyx\User\Api\UserController::class, 'delete']);
});

