<?php

namespace Onyx\User;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function register()
    {
        $this->app->make('Onyx\User\UserController');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'users');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang', 'user');

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '/routes/web.php';

        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'user');

        $this->publishes([
            __DIR__ . '/config/user.php' => config_path('user.php'),
            __DIR__ . '/config/menu.txt' => config_path('menu-items/user.txt'),
            __DIR__ . '/resources/lang' => resource_path('lang/user'),
            __DIR__ . '/resources/views' => resource_path('views/users'),
            __DIR__ . '/database/seeders' => database_path('seeders'),
        ]);

    }

}
