# User Package [onyx/users]

---

## Required packages

1. Roles package (onyx/roles)

---

## Installation

1. Add package to project composer - `composer require onyx/users`
2. Publish files from package - `php artisan vendor:publish`

